package gui;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.VPos;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.TextAlignment;
import util.BarcodeManager;

/**
 * MainForm FXML Controller class
 *
 * @author Danil
 */
public class MainFormController implements Initializable {

    @FXML
    BorderPane rootPane;
    @FXML
    TextField codeField;
    @FXML
    Canvas canvas;
    
    private static String TYPE_CODE128 = "TYPE_CODE128";
    private String code;
    private String type = TYPE_CODE128;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        rootPane.widthProperty().addListener(x -> {
            redraw();
        });
    }    
    
    public void redraw() {
        canvas.setWidth(rootPane.getWidth()-20);
        String text = code;
        GraphicsContext g = canvas.getGraphicsContext2D();
        g.setFill(Color.WHITE);
        g.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
        
        if (text == null || text.isEmpty()) {
            return;
        }
        
        g.setStroke(Color.BLACK);
        g.setTextAlign(TextAlignment.CENTER);
        g.setTextBaseline(VPos.CENTER);
            
        if (type == TYPE_CODE128) {
            ArrayList<Long> bitData = BarcodeManager.incodeCode128StartB(text);
            if (bitData == null) {
                new Alert(Alert.AlertType.WARNING, "Закодировать можно только символы ASCII (латинские)", ButtonType.OK)
                        .showAndWait();
                return;
            }
            
            int curPos = 0;
            int controlSum = 104;
            
            int posNum = (bitData.size())*11+20;
            curPos = 9;
            double bitWidth = canvas.getWidth() / (posNum);
        
            for (Long charData : bitData) {
                String curChar = charData.toString();
                double pos1 = curPos*bitWidth;
                for (char ch : curChar.toCharArray()) {
                    if (ch == '1') {
                        g.setLineWidth(bitWidth*1.1);
                        g.strokeLine(curPos*bitWidth, 10, curPos*bitWidth, canvas.getHeight()-50);
                    }
                    curPos += 1;
                }

                String key = BarcodeManager.getKeyCode128(charData);
                if (key.equals("Stop")) {
                    g.strokeLine(curPos*bitWidth, 10, curPos*bitWidth, canvas.getHeight()-50);
                    curPos++;
                    g.strokeLine(curPos*bitWidth, 10, curPos*bitWidth, canvas.getHeight()-50);
                    curPos++;
                }
                
                double pos2 = curPos*bitWidth;
                double w = pos2-pos1;

                g.setLineWidth(1);
                g.strokeText(key, pos1+w/2, canvas.getHeight()-25,w);

                g.setLineWidth(bitWidth);
                //g.strokeLine(pos1, canvas.getHeight()-40, pos1, canvas.getHeight()-10);
                g.strokeLine(pos2, canvas.getHeight()-40, pos2, canvas.getHeight()-10);
            }
        }
    }
    
    public void createBarcodeCode128() {
        code = codeField.getText();
        type = TYPE_CODE128;
        redraw();
    }
}

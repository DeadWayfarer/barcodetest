package util;

import java.util.*;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

/**
 *
 * @author Danil
 */
public class BarcodeManager {
    public static Map<String, Long> MAP_CODE128_CODES = new HashMap<>();
    public static Map<String, Integer> MAP_CODE128_VALUES = new HashMap<>();
    static {
        String[] rows = 
                (" 	11011001100\n" +
                "!	11001101100\n" +
                "\"	11001100110\n" +
                "#	10010011000\n" +
                "$	10010001100\n" +
                "%	10001001100\n" +
                "&	10011001000\n" +
                "'	10011000100\n" +
                "(	10001100100\n" +
                ")	11001001000\n" +
                "*	11001000100\n" +
                "+	11000100100\n" +
                ",	10110011100\n" +
                "-	10011011100\n" +
                ".	10011001110\n" +
                "/	10111001100\n" +
                "0	10011101100\n" +
                "1	10011100110\n" +
                "2	11001110010\n" +
                "3	11001011100\n" +
                "4	11001001110\n" +
                "5	11011100100\n" +
                "6	11001110100\n" +
                "7	11101101110\n" +
                "8	11101001100\n" +
                "9	11100101100\n" +
                ":	11100100110\n" +
                ";	11101100100\n" +
                "<	11100110100\n" +
                "=	11100110010\n" +
                ">	11011011000\n" +
                "?	11011000110\n" +
                "@	11000110110\n" +
                "A	10100011000\n" +
                "B	10001011000\n" +
                "C	10001000110\n" +
                "D	10110001000\n" +
                "E	10001101000\n" +
                "F	10001100010\n" +
                "G	11010001000\n" +
                "H	11000101000\n" +
                "I	11000100010\n" +
                "J	10110111000\n" +
                "K	10110001110\n" +
                "L	10001101110\n" +
                "M	10111011000\n" +
                "N	10111000110\n" +
                "O	10001110110\n" +
                "P	11101110110\n" +
                "Q	11010001110\n" +
                "R	11000101110\n" +
                "S	11011101000\n" +
                "T	11011100010\n" +
                "U	11011101110\n" +
                "V	11101011000\n" +
                "W	11101000110\n" +
                "X	11100010110\n" +
                "Y	11101101000\n" +
                "Z	11101100010\n" +
                "[	11100011010\n" +
                "\\	11101111010\n" +
                "]	11001000010\n" +
                "^	11110001010\n" +
                "_	10100110000\n" +
                "`	10100001100\n" +
                "a	10010110000\n" +
                "b	10010000110\n" +
                "c	10000101100\n" +
                "d	10000100110\n" +
                "e	10110010000\n" +
                "f	10110000100\n" +
                "g	10011010000\n" +
                "h	10011000010\n" +
                "i	10000110100\n" +
                "j	10000110010\n" +
                "k	11000010010\n" +
                "l	11001010000\n" +
                "m	11110111010\n" +
                "n	11000010100\n" +
                "o	10001111010\n" +
                "p	10100111100\n" +
                "q	10010111100\n" +
                "r	10010011110\n" +
                "s	10111100100\n" +
                "t	10011110100\n" +
                "u	10011110010\n" +
                "v	11110100100\n" +
                "w	11110010100\n" +
                "x	11110010010\n" +
                "y	11011011110\n" +
                "z	11011110110\n" +
                "{	11110110110\n" +
                "|	10101111000\n" +
                "}	10100011110\n" +
                "~	10001011110\n" +
                "US	10111101000\n" +
                "FNC 3	10111100010\n" +
                "FNC 2	11110101000\n" +
                "Shift B	11110100010\n" +
                "Code C	10111011110\n" +
                "Code B	10111101110\n" +
                "FNC 4	11101011110\n" +
                "FNC 1	11110101110\n" +
                "Start A	11010000100\n" +
                "Start B	11010010000\n" +
                "Start C	11010011100\n" +
                "Stop	11000111010"
                ).split("\n");
        int val = 0;
        for (String row : rows) {
            String[] pair = row.split("\t");
            MAP_CODE128_CODES.put(pair[0], Long.valueOf(pair[1]));
            MAP_CODE128_VALUES.put(pair[0], val++);
        }
        
        
    }
    
    public static int getValueCode128(String findKey) {
        return MAP_CODE128_VALUES.get(findKey);
    }
    
    public static Long getCodeCode128(String ch) {
        if (MAP_CODE128_CODES.containsKey(ch)) {
            return MAP_CODE128_CODES.get(ch);
        }
        return null;
    }
    
    public static Long getCodeCode128(int value) {
        String key = MAP_CODE128_VALUES
                .entrySet()
                .stream()
                .filter(x -> x.getValue() == value)
                .reduce((a,b) -> a).get().getKey();
        if (MAP_CODE128_CODES.containsKey(key)) {
            return MAP_CODE128_CODES.get(key);
        }
        return null;
    }
    
    public static String getKeyCode128(Long code) {
        String key = MAP_CODE128_CODES
                .entrySet()
                .stream()
                .filter(x -> x.getValue() == code)
                .reduce((a,b) -> a).get().getKey();
        return key;
    }
    
    public static ArrayList<Long> incodeCode128StartB(String msg) {
        int curPos = 0;
        int controlSum = 104;
        char[] chars = msg.toCharArray();
        ArrayList<Long> data = new ArrayList<>();
        data.add(BarcodeManager.getCodeCode128("Start B"));
        for (char ch : chars) {
            Long charData = BarcodeManager.getCodeCode128(""+ch);
            if (charData == 0) {
                return null;
            }
            data.add(charData);
            curPos++;
            controlSum += BarcodeManager.getValueCode128(""+ch)*curPos;
        }

        data.add(BarcodeManager.getCodeCode128(controlSum % 103));
        data.add(BarcodeManager.getCodeCode128("Stop"));

        
        return data;
    }
    
    public static Long toBin(int decimal) {
        long bin = 0;
        String binString = "";
        while (decimal > 0) {
            binString = (decimal & 1) + binString;
            decimal = decimal >> 1;
        }
        
        return Long.valueOf(binString);
    }
}
